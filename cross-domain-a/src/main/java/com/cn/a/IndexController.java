package com.cn.a;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.cn.utils.HttpClientUtils;


@Controller
@SpringBootApplication
public class IndexController {
    
	@RequestMapping("/a-index")
	public String index(){
		return "index";
	}
	/*
	 * httpclient转发到b
	 */
	@RequestMapping("/a-b-httpclient")
	@ResponseBody
	public JSONObject tob(){
		JSONObject result = HttpClientUtils.httpGet("http://127.0.0.1:8081/b-index");
	    System.err.println(result);
		return result;
	}
	public static void main(String[] args) {
		SpringApplication.run(IndexController.class,args);
	}
}
