<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
   $(function(){
	   //正常调用
	   $("#btn1").click(function(){
		   $.ajax({
				type : "POST",
				async : false,
				url : "http://127.0.0.1:8081/b-index",
				dataType : "json",
				success : function(data) {
					alert(data["info"]);
				},
				error : function() {
					alert("调用失败");
				}
			});
		});
	   //设置响应头
	   $("#btn2").click(function(){
		   $.ajax({
				type : "POST",
				async : false,
				url : "http://127.0.0.1:8081/b-index-response",
				dataType : "json",
				success : function(data) {
					alert(data["info"]);
				},
				error : function() {
					alert("调用失败");
				}
			});
		});
	   //jsonp
	   $("#btn3").click(function(){
		   $.ajax({
			    type : "GET",
				async : false,
				url : "http://127.0.0.1:8081/b-index-jsonp",
				dataType : "jsonp",
				jsonp : "jsonpCallback",//服务端用于接收callback调用的function名的参数 
				success : function(data) {
					alert(data["info"]);
				},
				error : function() {
					alert("调用失败");
				}
			});
		});
	   //httpclient
	   $("#btn4").click(function(){
		   $.ajax({
				type : "POST",
				async : false,
				url : "/a-b-httpclient",
				dataType : "json",
				success : function(data) {
					alert(data["info"]);
				},
				error : function() {
					alert("调用失败");
				}
			});
		});
   })
</script>
</head>
<body>
<button id="btn1">项目a正常访问项目b</button><br/><br/><br/>
<button id="btn2">设置响应头</button><br/><br/><br/>
<button id="btn3">jsonp</button><br/><br/><br/>
<button id="btn4">httpclient</button><br/><br/><br/>
</body>
</html>