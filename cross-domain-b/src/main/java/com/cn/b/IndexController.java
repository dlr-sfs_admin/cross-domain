package com.cn.b;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

@RestController
@SpringBootApplication
public class IndexController {
    /*
     * 用于返回数据给调用方
     */
	@RequestMapping("/b-index")
	public Map<String,String> returnMessage(){
		Map<String, String> map=new HashMap<String, String>();
		map.put("info", "测试成功");
		return map;
	}
	
	@RequestMapping("/b-index-response")
	public Map<String,String> returnMessageResponse(HttpServletResponse response){
		//允许浏览器跨域,允许所有域名都可以
		response.setHeader("Access-Control-Allow-Origin", "*");
		Map<String, String> map=new HashMap<String, String>();
		map.put("info", "测试成功,连接b项目");
		return map;
	}
	
	@RequestMapping("/b-index-jsonp")
	public void returnMessageJsonp(HttpServletResponse response,String jsonpCallback) throws Exception{
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		JSONObject json = new JSONObject();
		json.put("info", "测试成功,连接b项目");
		PrintWriter writer = response.getWriter();
		writer.print(jsonpCallback + "(" + json.toString() + ")");
		writer.close();
	}
	public static void main(String[] args) {
		SpringApplication.run(IndexController.class,args);
	}
}
